/* hwjs_07 assignment 2 */

function rotate(elem) {
	if (elem.classList.contains('active')) {
		elem.classList.remove('active')
	} else elem.classList.add('active')
}

/* hwjs_07 assignment 1 */

function toggle(elemId) {
	const elem = document.getElementById(elemId)
	const arrow = elem.querySelector('.item-arrow')
	const submenu = elem.querySelector('.menu-submenu')
	const submenuItemsCount = submenu.querySelectorAll('.submenu-item').length

	if (arrow) {
		rotate(arrow) 

		if (submenu) {
			console.log('submenu.classList', submenu.classList)
			if (submenu.classList.contains('active')) {
				submenu.classList.remove('active')
				submenu.style = ''
			} else {
				submenu.classList.add('active')
				submenu.style = `--height: ${submenuItemsCount * 40}px;`
			}
		}
	}
}