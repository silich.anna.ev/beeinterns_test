/* hwjs_06 assignment 1 */

const baseBlock = document.querySelector('#base') // find 'basic level' block
const cardsNames = baseBlock.getElementsByClassName('card-name') // find all cards names in 'basic level' block

for (let i = 0; i < cardsNames.length; i++) {
	let string = cardsNames[i].innerHTML // get every string from 'basic level' block
	string = string.toUpperCase() // convert string top upper case
	cardsNames[i].innerHTML = string // assign the result
}

/* hwjs_06 assignment 2 */

const cardsDescrip = baseBlock.getElementsByClassName('card-description') // find all cards descriptions in 'basic level' block

for (let i = 0; i < cardsDescrip.length; i++) {
	let string = cardsDescrip[i].innerHTML // get every string from 'basic level' block

	// if string length > 20
	if (string.length > 20) {
		string = string.substring(0, 20) // cut to 20 length
		cardsDescrip[i].innerHTML = string + '...' // add '...' at the end and assign the result
	}
}