/* hwjs_08 assignment 1 */

/* function to add all lectures to the block */
function addAll() {
	const allBlock = document.querySelector('#all-lectures')
	const itemsContainer = allBlock.querySelector('.course-items')

	/* clear if items exist */
	if (itemsContainer.childNodes.length > 0) {
		while (itemsContainer.firstChild) {
    itemsContainer.removeChild(itemsContainer.firstChild)
	 }
	}

	/* append items from all courses */
	const allItems = document.querySelectorAll('.item-card')
	for (let i = 0; i < allItems.length; i++) {
		itemsContainer.append(allItems[i].cloneNode(true))
	}
}

/* function to create block for all lectures */
function createAllBlock() {
	/* clone block from one of the courses */
	const baseBlock = document.querySelector('#js').cloneNode(true)
	baseBlock.id = 'all-lectures'

	/* set slide arguments */
	const arrows = baseBlock.querySelectorAll('.btn-arrow')
	arrows[0].setAttribute( "onClick", "slide('left', 'all-lectures')" )
	arrows[1].setAttribute( "onClick", "slide('right', 'all-lectures')" )

  /* set name and description */
	baseBlock.querySelector('.info-level').innerHTML = 'Все лекции на нашем сайте'
	baseBlock.querySelector('.info-name').innerHTML = 'All lectures'.toUpperCase() 

	/* set btn with items count */
	const lecturesNum = baseBlock.querySelector('.header-btns').childNodes[1]
	const itemsLength = document.querySelectorAll('.item-card').length
	lecturesNum.innerHTML = itemsLength + ' лекций'
	lecturesNum.setAttribute( "onClick", "addAll()")

	/* add cloned block */
	const mainContent = document.querySelector('.main-content')
	mainContent.prepend(baseBlock)

	/* add lectures to our new block */
	addAll()
}

/* hwjs_08 assignment 2 */

/* function to remake base html course to js/react course */
function addJSReact() {
	const jsBlock = document.querySelector('#base')
	jsBlock.id = 'js'

	/* set slide arguments */
	const arrows = jsBlock.querySelectorAll('.btn-arrow')
	arrows[0].setAttribute( "onClick", "slide('left', 'js')" )
	arrows[1].setAttribute( "onClick", "slide('right', 'js')" )

	/* set new name and description */
	jsBlock.querySelector('.info-level').innerHTML = 'Продвинутый уровень'
	jsBlock.querySelector('.info-name').innerHTML = 'Javascript/React'.toUpperCase() 

	/* set names and descriptions for lectures */
	const allTitles = jsBlock.querySelectorAll('.card-name')
	const allDescs = jsBlock.querySelectorAll('.card-description')

	allTitles[0].innerHTML = 'Event loop'
	allDescs[0].innerHTML = 'Event loop'
	allTitles[1].innerHTML = 'Асинхронный JavaScript'
	allDescs[1].innerHTML = 'Асинхронный JavaScript'
	allTitles[2].innerHTML = 'React Life Circle'
	allDescs[2].innerHTML = 'React Life Circle'
	allTitles[3].innerHTML = 'Redux.js'
	allDescs[3].innerHTML = 'Redux.js'
}

/* function to rename one of css lecture to html */
function renameCss() {
	const htmlBlock = document.querySelector('#pro')
	htmlBlock.id = 'html'

	/* set slide arguments */
	const arrows = htmlBlock.querySelectorAll('.btn-arrow')
	arrows[0].setAttribute( "onClick", "slide('left', 'html')" )
	arrows[1].setAttribute( "onClick", "slide('right', 'html')" )

	const htmlTitles = htmlBlock.querySelectorAll('.card-name')
	const htmnlDescs = htmlBlock.querySelectorAll('.card-description')

	htmlTitles[1].innerHTML = 'Создание HTML таблиц'
	htmnlDescs[1].innerHTML = 'Создание HTML таблиц: ряды, столбцы, ячейки'
}

/* function to add data-* attributes to the lectures */
function addData() {
	/* add to js/react block */
	const jsBlock = document.querySelector('#js')
	const jsCards = jsBlock.querySelectorAll('.item-card')

	jsCards[0].dataset.group = 'javascript'
	jsCards[1].dataset.group = 'javascript'
	jsCards[2].dataset.group = 'react'
	jsCards[3].dataset.group = 'react'

	/* add to html/css block */
	const htmlBlock = document.querySelector('#html')
	const htmlCards = htmlBlock.querySelectorAll('.item-card')

	htmlCards[0].dataset.group = 'html'
	htmlCards[1].dataset.group = 'html'
	htmlCards[2].dataset.group = 'css'
	htmlCards[3].dataset.group = 'css'
}

function createBlocks() {
	addJSReact()
	renameCss()
	addData()
  createAllBlock()
}

/* function to filter all lectures */
function filter(type) {
	addAll()

	const allBlock = document.querySelector('#all-lectures')
	const itemsContainer = allBlock.querySelector('.course-items')
	const items = allBlock.querySelectorAll('.item-card')

	const result = document.createElement('div')
	result.classList.add('course-items')

	for (let i = 0; i < items.length; i++) {
		if (items[i].dataset.group == type) result.append(items[i])
	}
  
	itemsContainer.replaceWith(result)
}

/* function to add filter btns */
function addFilters() {
	const allBlock = document.querySelector('#all-lectures')
	const btns = allBlock.querySelector('.header-btns')

	const jsFilter = btns.childNodes[1].cloneNode(true)
	jsFilter.innerHTML = 'Javascript'
	jsFilter.setAttribute( "onClick", "filter('javascript')" )

	const reactFilter = btns.childNodes[1].cloneNode(true)
	reactFilter.innerHTML = 'React'
	reactFilter.setAttribute( "onClick", "filter('react')" )

	const cssFilter = jsFilter.cloneNode(true)
	cssFilter.innerHTML = 'CSS'
	cssFilter.setAttribute( "onClick", "filter('css')" )

	const htmlFilter = jsFilter.cloneNode(true)
	htmlFilter.innerHTML = 'HTML'
	htmlFilter.setAttribute( "onClick", "filter('html')" )

	btns.prepend(reactFilter)
	btns.prepend(jsFilter)
	btns.prepend(cssFilter)
	btns.prepend(htmlFilter)
}