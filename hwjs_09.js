/* hwjs_09 assignment */

function formObject() {
	const lecturesObj = {}

	const allBlock = document.querySelector('#all-lectures')
	const allItems = allBlock.querySelectorAll('.item-card')

	for (let i = 0; i < allItems.length; i++) {
		const lecture = {
			title: allItems[i].querySelector('.card-name').innerHTML,
			description: allItems[i].querySelector('.card-description').innerHTML,
			date: allItems[i].querySelector('.card-date').innerHTML,
			image: allItems[i].querySelector('.card-img').src,
			label: allItems[i].querySelector('.card-type').innerHTML
		}
		
		if (lecturesObj.hasOwnProperty(allItems[i].dataset.group)) { 
			lecturesObj[allItems[i].dataset.group].push(lecture)
		} else {
			lecturesObj[allItems[i].dataset.group] = []
			lecturesObj[allItems[i].dataset.group].push(lecture)
		}
	}

	console.log('lecturesObj', lecturesObj)
}