let positions = { 
	'all-lectures': 1,
	js: 1,
	html: 1
}

function slide(direction, collection) {
	const el = document.querySelector('#' + collection + ' .course-items')
	const pos = positions[collection]

	if (direction === 'left') {
		const scrollTo = el.querySelector('.item-card:nth-of-type(' + (pos - 1) + ')')
		
		if (!scrollTo) {
			return
		}

		scrollTo.scrollIntoView({ inline: 'start', block: "center"})
		positions[collection]--


	}
	else {
		const scrollTo = el.querySelector('.item-card:nth-of-type(' + (pos + 1) + ')')

		if (!scrollTo) {
			return
		}

		scrollTo.scrollIntoView({ inline: 'start', block: "center"})
		positions[collection]++
	}
}

window.onload = function() {
	createBlocks()
	addFilters()
	formObject()
}